use clap::Parser;
use futures::future;
use hyper::{server::conn::http1, service::service_fn};
use hyper_util::rt::TokioIo;
use tokio::{net::TcpListener, time::interval};
use troll_patrol::{request_handler::*, *};

use serde::Deserialize;
use sled::Db;
use std::{
    collections::{BTreeMap, HashMap, HashSet},
    fs::File,
    io::BufReader,
    net::SocketAddr,
    path::PathBuf,
    time::Duration,
};
use tokio::{
    signal, spawn,
    sync::{broadcast, mpsc, oneshot},
    time::sleep,
};

mod command;
use command::Command;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Name/path of the configuration file
    #[arg(short, long, default_value = "config.json")]
    config: PathBuf,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Config {
    pub db: DbConfig,
    // map of distributor name to IP:port to contact it
    pub distributors: BTreeMap<BridgeDistributor, String>,
    extra_infos_base_url: String,

    // confidence required to consider a bridge blocked
    confidence: f64,

    // block open-entry bridges if they get more negative reports than this
    max_threshold: u32,

    // block open-entry bridges if they get more negative reports than
    // scaling_factor * bridge_ips
    //Not used
    // scaling_factor: f64,

    // minimum number of historical days for statistical analysis
    min_historical_days: u32,

    // maximum number of historical days to consider in historical analysis
    max_historical_days: u32,

    //require_bridge_token: bool,
    port: u16,
    //config for updater
    update_interval: u64,
}

#[derive(Debug, Clone, Deserialize)]
pub struct DbConfig {
    // The path for the server database, default is "server_db"
    pub db_path: String,
}

impl Default for DbConfig {
    fn default() -> DbConfig {
        DbConfig {
            db_path: "server_db".to_owned(),
        }
    }
}

async fn update_daily_info(db: &Db, config: Config) -> HashMap<[u8; 20], HashSet<String>> {
    update_extra_infos(db, &config.extra_infos_base_url)
        .await
        .unwrap();
    update_negative_reports(db, &config.distributors).await;
    let new_blockages = guess_blockages(
        db,
        // Using max_threshold for convenience
        &lox_analysis::LoxAnalyzer::new(config.max_threshold),
        config.confidence,
        config.min_historical_days,
        config.max_historical_days,
    );
    report_blockages(&config.distributors, new_blockages.clone()).await;

    // Generate tomorrow's key if we don't already have it
    match get_negative_report_public_key(db) {
        Ok(_pubkey) => _pubkey,
        Err(e) => panic!("Error getting negative report key {:?}", e),
    };

    // Return new detected blockages
    println!("Finished daily update, returning new_blockages");
    new_blockages
}

async fn run_updater(
    update_interval: u64,
    updater_tx: mpsc::Sender<Command>,
    mut kill: broadcast::Receiver<()>,
) {
    tokio::select! {
        updater = run_update(update_interval, updater_tx) => updater,
        _ = kill.recv() => {println!("Shut down updater");},
    }
}

async fn run_update(update_interval: u64, tx: mpsc::Sender<Command>) {
    // Run updater once per interval days
    let mut interval = interval(Duration::from_secs(update_interval));
    loop {
        interval.tick().await;
        let cmd = Command::Update {};
        if let Err(err) = tx.send(cmd).await {
            println!("Error sending resources to Lox BridgeDB: {:?}", err)
        };
        sleep(Duration::from_secs(1)).await;
    }
}

async fn create_context_manager(
    config: Config,
    context_rx: mpsc::Receiver<Command>,
    mut kill: broadcast::Receiver<()>,
) {
    tokio::select! {
        create_context = context_manager(config, context_rx) => create_context,
        _ = kill.recv() => {println!("Shut down manager");},
    }
}

async fn context_manager(config: Config, mut context_rx: mpsc::Receiver<Command>) {
    let db: Db = sled::open(&config.db.db_path).unwrap();

    // Create negative report key for today if we don't have one
    match get_negative_report_public_key(&db) {
        Ok(_pubkey) => _pubkey,
        Err(e) => {
            panic!("Error: {:?}", e);
        }
    };

    while let Some(cmd) = context_rx.recv().await {
        use Command::*;
        match cmd {
            Request { req, sender } => {
                let response = handle(&db, req).await;
                if let Err(e) = sender.send(response) {
                    eprintln!("Server Response Error: {:?}", e);
                };
                sleep(Duration::from_millis(1)).await;
            }
            Shutdown { shutdown_sig } => {
                println!("Sending Shutdown Signal, all threads should shutdown.");
                drop(shutdown_sig);
                println!("Shutdown Sent.");
            }
            Update {} => {
                update_daily_info(&db, config.clone()).await;
                sleep(Duration::from_millis(1)).await;
            }
        }
    }
}

#[tokio::main]
async fn main() {
    let args: Args = Args::parse();

    let config: Config = serde_json::from_reader(BufReader::new(
        File::open(&args.config).expect("Could not read config file"),
    ))
    .expect("Reading config file from JSON failed");

    let (request_tx, request_rx) = mpsc::channel(32);

    let updater_tx = request_tx.clone();
    let shutdown_cmd_tx = request_tx.clone();

    // create the shutdown broadcast channel
    let (shutdown_tx, mut shutdown_rx) = broadcast::channel(16);
    let kill = shutdown_tx.subscribe();
    let mut kill_requests = shutdown_tx.subscribe();
    let kill_updater = shutdown_tx.subscribe();

    // Listen for ctrl_c, send signal to broadcast shutdown to all threads by dropping shutdown_tx
    let shutdown_handler = spawn(async move {
        tokio::select! {
            _ = signal::ctrl_c() => {
                let cmd = Command::Shutdown {
                    shutdown_sig: shutdown_tx,
                };
                shutdown_cmd_tx.send(cmd).await.unwrap();
                sleep(Duration::from_secs(1)).await;

                _ = shutdown_rx.recv().await;
            }
        }
    });

    let update_handler =
        spawn(async move { run_updater(config.update_interval, updater_tx, kill_updater).await });

    let addr = SocketAddr::from(([0, 0, 0, 0], config.port));

    let context_manager =
        spawn(async move { create_context_manager(config, request_rx, kill).await });

    let listener = TcpListener::bind(addr).await.expect("failed to bind");

    let svc_fn = move |req| {
        let request_tx = request_tx.clone();
        let (response_tx, response_rx) = oneshot::channel();
        let cmd = Command::Request {
            req,
            sender: response_tx,
        };
        async move {
            if let Err(err) = request_tx.send(cmd).await {
                println!("Error sending http request to handler{:?}", err);
            }
            response_rx.await.unwrap()
        }
    };

    let request_handler = spawn(async move {
        loop {
            tokio::select! {
                res = listener.accept() => {
                    let (stream, _) = res.expect("Failed to accept");
                    let io = TokioIo::new(stream);
                    let handler = svc_fn.clone();
                    spawn(async move {
                        if let Err(err) = http1::Builder::new().serve_connection(io, service_fn(handler)).await {
                            println!("Error serving connection: {:?}", err);
                        }
                    });
                }
                _ = kill_requests.recv() => {
                    println!("Shut down request_handler");
                    break;
                }
            }
        }
    });

    println!("Listening on {}", addr);

    future::join_all([
        context_manager,
        update_handler,
        request_handler,
        shutdown_handler,
    ])
    .await;
}
