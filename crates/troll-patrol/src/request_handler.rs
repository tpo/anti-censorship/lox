use crate::{negative_report::EncryptedNegativeReport, *};
use http::header::ACCEPT;
use http_body_util::{combinators::BoxBody, BodyExt, Full};
use hyper::{body::Body, header::HeaderValue, Method, Request, Response, StatusCode};
use serde_json::json;
use sled::Db;
use std::{convert::Infallible, fmt::Debug};

// Handle submitted reports
pub async fn handle<B: Body>(
    db: &Db,
    req: Request<B>,
) -> Result<Response<BoxBody<Bytes, Infallible>>, Infallible>
where
    <B as Body>::Error: Debug,
{
    match req.method() {
        &Method::OPTIONS => Ok(Response::builder()
            .header("Access-Control-Allow-Origin", HeaderValue::from_static("*"))
            .header("Access-Control-Allow-Headers", "accept, content-type")
            .header("Access-Control-Allow-Methods", "POST")
            .status(StatusCode::OK)
            .body(full("Allow POST"))
            .unwrap()),
        _ => match (req.method(), req.uri().path()) {
            (&Method::POST, "/negativereport") => Ok::<_, Infallible>({
                let bytes = req.into_body().collect().await.unwrap().to_bytes();
                // We cannot depend on the transport layer providing E2EE, so
                // negative reports should be separately encrypted.
                let enr: EncryptedNegativeReport = match bincode::deserialize(&bytes) {
                    Ok(enr) => enr,
                    Err(e) => {
                        let response = json!({"error": e.to_string()});
                        let val = serde_json::to_string(&response).unwrap();
                        return Ok(prepare_header(val));
                    }
                };
                handle_encrypted_negative_report(db, enr);
                prepare_header("OK".to_string())
            }),
            _ => {
                // Return 404 not found response.
                Ok(Response::builder()
                    .status(StatusCode::NOT_FOUND)
                    .body(full("Not found"))
                    .unwrap())
            }
        },
    }
}

fn full<T: Into<Bytes>>(chunk: T) -> BoxBody<Bytes, Infallible> {
    Full::new(chunk.into()).boxed()
}

// Prepare HTTP Response for successful Server Request
pub fn prepare_header(response: String) -> Response<BoxBody<Bytes, Infallible>> {
    let mut resp = Response::new(full(response));
    resp.headers_mut()
        .insert(ACCEPT, "application/json".parse().unwrap());
    resp.headers_mut()
        .insert("Access-Control-Allow-Origin", HeaderValue::from_static("*"));
    resp
}
