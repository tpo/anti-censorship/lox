# Changelog

Entries are listed in reverse chronological order.


## 0.2.0

* Remove serde_with and downgrade chrono
* Downgrade specific versions to match browser integration
* Add structs to support update_cred and update_invite protocols
* Add (de)serialize invite as a base64 string
* Add return values for next unlock function
* Add Lox system info struct
* Add server constants


## 0.1.0

* Initial Release
