use super::bridge_table::BridgeLine;
use curve25519_dalek::Scalar;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;

/// Information that needs to be known to verify a Troll Patrol report
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BridgeVerificationInfo {
    /// BridgeLine for this bridge
    pub bridge_line: BridgeLine,

    /// Buckets containing this bridge if this bridge is a Lox bridge
    pub buckets: HashSet<Scalar>,
}
